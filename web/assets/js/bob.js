function read_input(id){

    return document.getElementById(id).value;

}


function do_the_bob(text){

    var output = "";
    var capital = false;
    var exceptions = [
        " ",
        ".",
        ":",
        ",",
        ";",
        "-",
        "_",
        "?",
        "!",
        "#",
        "+",
        "*",
        "'",
        "\"",
        "\'"
    ];
    
    for(var i=0; i<text.length; i++){

        var char = text.charAt(i);
        var exception = false;

        for(var ii in exceptions){

            if(char == exceptions[ii]){
                exception = true;
                break;
            }

        }

        if(exception){

            output += char;

        }else{

            var new_char = null;
            var new_capital = null;

            if(capital){

                new_char = char.toLowerCase();
                new_capital = false;

            }else{

                new_char = char.toUpperCase();
                new_capital = true;

            }

            output += new_char;
            capital = new_capital;

        }

    }

    return output

}


function text_changed(){

    var in_text = read_input("in_text");

    var bob_text = do_the_bob(in_text);

    document.getElementById("out_text").value = bob_text;

}